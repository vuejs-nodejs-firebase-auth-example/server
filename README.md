# server

#  API

## Description

This is our Nodejs backend used for the Auth with firebase

## Update the config

First update the config file with the accurate variables &nbsp; 

- path:&nbsp;  ./config/serviceAccountKey.json &nbsp; 

Update the DB URL to the accurate URL inside the app.js file

## Installation and Running the server

$ npm install &nbsp;  

$ DEBUG=server:* nodemon start

## Accessibility of our route

- http://localhost:3000/
